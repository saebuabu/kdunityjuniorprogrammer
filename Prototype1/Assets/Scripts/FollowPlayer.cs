using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPlayer : MonoBehaviour
{
    //Dit gameobject wordt via de inspector gekoppeld aan de tank gameobject
    //Door een public property toe te voegen aan de class wordt ie toegevoed aan de inspector
    public GameObject player;
    
    //Hoe is de camera relatief gepositioneerd tov de vehikel
    private Vector3 offset = new Vector3(0, 6, -11);

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void LateUpdate()
    {
        // de positie van de camera tov de tank is fixed mbv offset
        transform.position = player.transform.position + offset;
    }
}
