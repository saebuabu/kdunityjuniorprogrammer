using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float speed = 20.0f;
    public float turnSpeed = 45f;
    public float horizontalInput;
    public float forwardInput;
    public AudioSource m_MyAudioSource;
    public bool playing = false;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //naar links of rechts met de pijltjes toetsen
        horizontalInput = Input.GetAxis("Horizontal");
        forwardInput = Input.GetAxis("Vertical");

        //audioclip spelen als de tank beweegt
        if (forwardInput != 0)
        {
            if (!playing)
            {
                m_MyAudioSource.Play();
                playing = true;
            }
        }
        else
        {
            m_MyAudioSource.Pause();
            playing = false;
        }

        //We laten de tank in de z-as vooruit gaan
        //Verkorte notatie van transform.Translate(0,0,1);
        transform.Translate(Vector3.forward * Time.deltaTime * speed * forwardInput);
        // param 1 is de richting, param 2 is de hoek.
        transform.Rotate(Vector3.up, Time.deltaTime * turnSpeed * horizontalInput);

    }
}
